const face = {
    parts : {
        edge : {
            mapping : ["contour_left1","contour_left2","contour_left3","contour_left4","contour_left5","contour_left6","contour_left7","contour_left8","contour_left9",
                "contour_chin","contour_right9","contour_right8","contour_right7","contour_right6","contour_right5","contour_right4","contour_right3","contour_right2","contour_right1"],
            include : true
        },
        rightEye : {
            mapping : ["right_eye_left_corner","right_eye_lower_left_quarter","right_eye_bottom","right_eye_lower_right_quarter",
                "right_eye_right_corner","right_eye_upper_right_quarter","right_eye_top","right_eye_upper_left_quarter"],
            include : false
        },
        leftEye : {
            mapping : ["left_eye_left_corner","left_eye_lower_left_quarter","left_eye_bottom","left_eye_lower_right_quarter",
                "left_eye_right_corner","left_eye_upper_right_quarter","left_eye_top","left_eye_upper_left_quarter"],
            include : false
        },
        rightEyebrow : {
            mapping : ["right_eyebrow_left_corner","right_eyebrow_lower_left_quarter","right_eyebrow_lower_middle","right_eyebrow_lower_right_quarter",
                "right_eyebrow_right_corner","right_eyebrow_upper_right_quarter","right_eyebrow_upper_middle","right_eyebrow_upper_left_quarter"],
            include : false
        },
        leftEyebrow : {
            mapping : ["left_eyebrow_left_corner","left_eyebrow_lower_left_quarter","left_eyebrow_lower_middle","left_eyebrow_lower_right_quarter",
                "left_eyebrow_right_corner","left_eyebrow_upper_right_quarter","left_eyebrow_upper_middle","left_eyebrow_upper_left_quarter"],
            include : false
        },
        mouth : {
            mapping : ["mouth_left_corner","mouth_lower_lip_left_contour2","mouth_lower_lip_left_contour3","mouth_lower_lip_bottom",
                "mouth_lower_lip_right_contour3","mouth_lower_lip_right_contour2","mouth_right_corner","mouth_upper_lip_right_contour2",
                "mouth_upper_lip_right_contour1","mouth_upper_lip_top","mouth_upper_lip_left_contour1","mouth_upper_lip_left_contour2"],
            include : false
        }
    }
};


class FacePlusPlus {
    constructor(video, API_KEY, API_SECRET) {
        this.video = video;
        this.img = new Image();
        this.pureImageData = null;
        this.API_KEY = API_KEY;
        this.API_SECRET = API_SECRET;
        this.canvas = null;
        this.color = null;
        this.faceData = null;
        this.faceCoords = [];
        this.frameProcessor = null;

        this.formData = new FormData();
        this.regExp = new RegExp(/^data:image\/(png|jpg|jpeg);base64,/);
    }

    getHeadpose() {
        this.xhrHandler(0, 'headpose')
            .then(res => {
                const parsed = JSON.parse(res);
                const headpose = parsed.faces[0].attributes.headpose;
                const { yaw_angle, pitch_angle, roll_angle } = headpose;

                console.log(headpose);
                if (yaw_angle > 20 || yaw_angle < -20 ||
                    pitch_angle > 20 || pitch_angle < -20 ||
                    roll_angle > 15 || roll_angle < -15) {

                    this.hideCanvas();
                    this.resumeVideoStream();
                    alert('Please retake photo or upload new one!');
                } else {
                    this.getLandmarks();
                }
            });
    }

    getLandmarks() {
        this.xhrHandler(1)
            .then(res => {
                const parsed = JSON.parse(res);

                this.faceData = parsed.faces;
                this.computeFaceCoords();
            });
    }

    getRawFaceData() {
        return this.faceData[0];
    }

    getFaceCoords() {
        return this.faceCoords;
    }

    drawLandmarksOnCanvas() {
        const ctx = this.canvas.getContext('2d');

        ctx.font = "10px Arial";

        function drawRect(x, y, fillStyle) {
            ctx.fillStyle = fillStyle;
            ctx.fillRect(x - 2, y - 2, 4, 4);
        }

        function drawContour(contour){
            let color = getRandomColor();

            for(let i = 0; i < contour.length; i++) {
                drawRect(contour[i][0], contour[i][1], color);
                ctx.fillText(i.toString(), contour[i][0] + 3, contour[i][1] + 3);
            }
        }

        this.faceCoords[0].forEach(contour => drawContour(contour));
        this.faceCoords[1].forEach(contour => drawContour(contour));
    }

    computeFaceCoords() {
        console.log('compute face coords');
        const contours = {};
        const includeArr = [];
        const excludeArr = [];

        let source = {};

        this.faceCoords = [];

        if (this.faceData !== null) source = this.faceData[0].landmark;
        else throw new Error('Data from Face++ server is not received');

        for (let part in face.parts) {
            console.log(part);

            contours[part] = [];
        }

        console.log(source);

        for (let part in face.parts){
            for (let i = 0; i < face.parts[part].mapping.length; i++){
                console.log(face.parts[part].mapping[i]);

                contours[part].push([source[face.parts[part].mapping[i]].x, source[face.parts[part].mapping[i]].y]);
            }
        }

        console.log(`Face Coords data is computed`);

        for (let part in face.parts) {
            if (face.parts[part].include) {
                includeArr.push(contours[part]);
            } else {
                excludeArr.push(contours[part]);
            }
        }

        this.frameProcessor.addForehead(includeArr[0], excludeArr[0], excludeArr[1]);

        this.faceCoords = [includeArr, excludeArr];
    }

    init() {
        this.initCanvas();
        this.initEvents();
    }

    startVideoStream() {
        const constraints = { audio: false, video: { facingMode: "user" } };

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia(constraints)
                .then(stream => {
                    /*this.video.src = window.URL.createObjectURL(stream);
                    this.video.play();*/
                    window.stream = stream;
                    this.video.srcObject = stream;
                    //this.video.play();
                })
                .catch(() => alert('Streaming denied, use UPLOAD PHOTO button!'));
        }
    }

    stopVideoStream() {
        this.video.pause();
        this.video.style.display = 'none';
    }

    resumeVideoStream() {
        this.video.style.display = 'block';
        this.startVideoStream();
    }

    getBase64FromCanvas() {
        const imageBase = this.canvas.toDataURL().replace(this.regExp, '');

        this.formData.delete('image_base64');
        this.formData.append('image_base64', imageBase);
    }

    getBase64FromFile(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            this.formData.delete('image_base64');

            reader.readAsDataURL(file);
            reader.onload = () => {
                this.img.src = reader.result;
                resolve(reader.result.replace(this.regExp, ''));
            };
            reader.onerror = error => reject(error);
        });
    }

    xhrHandler(landmarkParam, attributes) {
        let returnAttributes;

        if (attributes) {
            returnAttributes = `return_attributes=${attributes}`;
            landmarkParam = 0;
        }

        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            const reqUrlArr = [
                `https://api-us.faceplusplus.com/facepp/v3/detect?`,
                `api_key=${this.API_KEY}&`,
                `api_secret=${this.API_SECRET}&`,
                `return_landmark=${landmarkParam}&`,
                `${returnAttributes}`
            ];
            const url = reqUrlArr.join('');

            xhr.open('POST', url, true);

            xhr.onload = function() {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    let error = new Error(xhr.statusText);
                    error.code = xhr.status;
                    reject(error);
                }
            };

            xhr.onerror = function() {
                reject(new Error("Network Error"));
            };

            xhr.send(this.formData);
        });
    }

    initCanvas() {
        const canvas = document.createElement('canvas');

        this.canvas = canvas;

        canvas.setAttribute('id', 'canvasOutput');

        this.hideCanvas();
        this.video.after(canvas);
    }

    renderCanvas(source) {
        const context = this.canvas.getContext('2d');
        let height = parseInt(getComputedStyle(source).height);
        let width = parseInt(getComputedStyle(source).width);

        this.pureImageData = null;

        context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        if (source.id && source.id === 'videoInput') {
            this.canvas.setAttribute('width', width + 'px');
            this.canvas.setAttribute('height', height + 'px');
            context.drawImage(source, 0, 0, width, height);
        } else {
            height = source.naturalHeight;
            width = source.naturalWidth;

            if (width > 640) {
                const scale = width / height;

                width = 640;
                height = width / scale;
            }

            this.canvas.setAttribute('width', width + 'px');
            this.canvas.setAttribute('height', height + 'px');
            context.drawImage(source, 0, 0, width, height);
        }

        this.pureImageData = context.getImageData(0, 0, width, height).data;

        this.frameProcessor = new OpenCVProcessor(parseInt(width), parseInt(height));

        this.canvas.style.display = 'block';
    }

    hideCanvas() {
        const ctx = this.canvas.getContext('2d');

        this.canvas.removeAttribute('width');
        this.canvas.removeAttribute('height');

        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.canvas.style.display = 'none';
    }

    applyMakeup(color) {
        if (this.pureImageData === null) {
            alert('Please take photo or upload yours!');
            return;
        }

        const ctx = this.canvas.getContext('2d');
        const width = parseInt(getComputedStyle(this.canvas).width);
        const height = parseInt(getComputedStyle(this.canvas).height);
        const frame = new cv.Mat(height, width, cv.CV_8UC4);

        ctx.drawImage(this.canvas, 0, 0, width, height);

        frame.data.set(this.pureImageData);

        const processed = this.frameProcessor.processMat(frame, this.faceCoords[0], this.faceCoords[1], color, 2);
        cv.imshow(this.canvas, processed);

        frame.delete();
        processed.delete();
    }

    initEvents() {
        const uploadFile = document.getElementById('uploadFile');
        const fileSource = document.getElementById('fileInput');
        const snapShot = document.getElementById('snap');
        const resume = document.getElementById('resume');
        const colorPanel = document.getElementById('colorPanel');

        this.startVideoStream(this.video);

        uploadFile.addEventListener('click', () => fileSource.click());

        fileSource.addEventListener('change', e => {
            const file = e.target.files[0];

            // create base64 from attached file (id=fileInput)
            this.getBase64FromFile(file)
                .then(imageBase => {
                    this.formData.append('image_base64', imageBase);

                    this.stopVideoStream();
                    this.renderCanvas(this.img);
                    this.getHeadpose();
                });
        });

        colorPanel.addEventListener('click', e => {
            const target = e.target;

            if (target.dataset !== undefined && target.dataset.color) {
                const arr = target.dataset.color.split(',');

                this.color = arr.map(item => parseInt(item));
            } else if (target.dataset !== undefined && target.dataset.resetter) {
                this.color = null;
            }

            if (this.color !== null) this.applyMakeup(this.color);
            else {
                alert('Please retake the photo or upload new one!');
                this.hideCanvas();
                this.resumeVideoStream();
            }
        });

        snapShot.addEventListener('click', () => {
            this.renderCanvas(this.video);
            this.getBase64FromCanvas();
            this.stopVideoStream();
            this.getHeadpose();
        });

        resume.addEventListener('click', () => {
            this.hideCanvas();
            this.resumeVideoStream();
        });

        console.log('Events initialized');
    }
}


document.addEventListener('DOMContentLoaded', () => {
    console.log('Application started');

    const API_KEY = '7HdQGZ03LIwYGaTus85FmTm60sscDnND';
    const API_SECRET = 'Ru9eOM5oQd0XV94D3o59Dkujgpz_hnM9';
    const video = document.getElementById('videoInput');

    window.facePlusPlus = new FacePlusPlus(video, API_KEY, API_SECRET);

    facePlusPlus.init();
});
