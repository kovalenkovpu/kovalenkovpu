document.addEventListener('DOMContentLoaded', () => {
    const photos = document.getElementById('photos');
    const colorPanel = document.getElementById('colorPanel');
    const resume = document.getElementById('resume');
    const FPP = window.facePlusPlus;

    function toggleAllPhotoClasses(target) {
        const imgs = photos.querySelectorAll('.photo');

        for (let img of imgs) {
            if (img.dataset.photo === target.dataset.photo) {
                img.classList.add('photo__choosed');
            } else img.classList.remove('photo__choosed');
        }
    }

    function toggleColorClasses(target) {
        const colors = colorPanel.querySelectorAll('.color');

        for (let color of colors) {
            if (color.dataset.color === target.dataset.color) {
                color.classList.add('color__active');
            } else color.classList.remove('color__active');
        }
    }

    photos.addEventListener('click', e => {
        const target = e.target;

        if (target.className === 'photo') {
            toggleAllPhotoClasses(target);
            FPP.stopVideoStream();
            FPP.hideCanvas();
            FPP.renderCanvas(target);
            FPP.getBase64FromCanvas();
            FPP.getHeadpose();
        }
    });

    colorPanel.addEventListener('click', e => {
        const target = e.target;

        if (target.className === 'color__active_border') {
            toggleColorClasses(target);
        }
    });

    resume.addEventListener('click', e => {
        const target = e.target;

        toggleAllPhotoClasses(target);
    });
});